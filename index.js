'use strict';

process.title = 'skittybot';

const snekfetch = require('snekfetch');
const htmlToText = require('html-to-text');

const INDICATOR_TOKENS = [
    'official',
    'pony',
    'poni',
    'thread',
    'bread',
    '/b/read'
];

const BOARD_ID = `trash`;
const API = `a.4cdn.org`;
const API_THREAD_STATS = `https://${API}/${BOARD_ID}/threads.json?s=ThreadStats`;
const API_CATALOG = `https://${API}/${BOARD_ID}/catalog.json`;
const APP_ERROR = -1;

const NETWORK_ERRORS = ['ETIMEDOUT', 'ENOTFOUND', 'ECONNREFUSED', 'ECONNRESET'];
const SIGNALS = ['SIGTERM', 'SIGHUP', 'SIGINT', 'SIGBREAK', 'SIGQUIT'];

function handleUncaughtError(error) {
    let log = console.error;
    if (error.code && ~NETWORK_ERRORS.indexOf(error.code)) log = console.info;
    let message, ref, stack;
    stack = (ref = error.stack) !== null ? ref : `${error.name}: ${error.message}`;
    message = `Uncaught Exception:\n  ${stack}`;
    log(`An uncaught exception occurred in the main process ${message}`);
}

process.on('uncaughtException', handleUncaughtError);
process.on('unhandledRejection', handleUncaughtError);
process.on('warning', warning => console.warn(warning.stack));

// Exit on signals
SIGNALS.forEach(signal => {
    process.on(signal, () => {
        console.log(`Shutting down.`);
        process.exit(0);
    });
});

class net_work {

    constructor() {
        this.current = null;
    }

    init() {
        this.id = setInterval(this.check.bind(this), 60000 * 60);
        process.nextTick(this.check.bind(this));
    }

    stop() {
        if (this.id) clearInterval(this.id);
    }

    getPageByID(target_id) {
        return new Promise(resolve => {
            snekfetch.get(API_THREAD_STATS)
                .catch(e => {
                    console.error(e);
                    resolve(APP_ERROR);
                })
                .then(result => {
                    if (typeof result === 'object') {
                        let pages = result.body;
                        for (let page in pages) {
                            if (typeof pages[page].threads === 'object' && pages[page].threads[target_id]) {
                                resolve(pages[page].page);
                                return;
                            }
                        }
                        resolve(APP_ERROR);
                    } else {
                        resolve(APP_ERROR);
                    }
                });
        });
    }

    findThread(comment_tokens) {
        return new Promise(resolve => {
            snekfetch.get(API_CATALOG)
                .then(res => {
                    if (typeof res !== 'object' && !Array.isArray(res.body)) {
                        resolve(APP_ERROR);
                        return;
                    }
                    let body = res.body;
                    let now = +new Date();
                    let oldest = now;
                    let OP;

                    let matches = [];

                    for (let page in body) {
                        for (let thread in body[page].threads) {

                            OP = body[page].threads[thread];
                            OP.com = htmlToText.fromString(OP.com);

                            let split = OP.com.replace(/\n/g, ' ').split(' ');
                            let tokens = [];

                            split.forEach(word => tokens.push(word.toLowerCase().replace(/[#()\\/.,!?;:]/g, '')));

                            tokens = tokens.filter((value, index, self) => {
                                if (!value) return false;
                                if (~value.indexOf('http')) return false;
                                if (self.indexOf(value) === index) return true;
                                return false;
                            });

                            if (!tokens.length) continue;

                            let hit = 0;

                            comment_tokens.forEach(token => {
                                if (tokens.includes(token))++hit;
                            });

                            if (hit) {
                                let conf = (100 / comment_tokens.length) * (hit / 100);
                                if (conf >= 0.4 && !OP.imagelimit) {

                                    if (OP.tim < oldest) oldest = OP.tim;

                                    matches.push({
                                        ID: OP.no,
                                        replies: OP.replies,
                                        created: OP.tim,
                                        last_modified: OP.last_modified,
                                        confidence: conf / 4
                                    });
                                }
                            }
                        }
                    }

                    let max_alive = (now - oldest) / 100000;

                    matches.forEach(thread => {
                        let score = (100 / max_alive) * ((now - thread.created) / 100000 / 100) / 100;
                        let p = score * 100;
                        thread.confidence += (p - score) / 4;
                    });

                    if (!matches.length) {
                        resolve(APP_ERROR);
                    } else {
                        resolve(matches);
                    }

                });
        });
    }

    filterByNameSync(threads) {
        return new Promise(async resolve => {
            let targets = [];
            await threads.reduce((p, c) => p.then(async () => {
                await new Promise(r => {
                    setTimeout(() => {
                        console.log(`checking ${c.ID}`);
                        snekfetch
                            .get(`https://namesync.net/namesync/qp.php?t=${c.ID}&b=${BOARD_ID}`)
                            .set('X-Requested-With', 'NameSync4.9.3')
                            .catch(console.error)
                            .then(res => {
                                if (typeof res === 'object') {
                                    let names = res.body;
                                    if (Array.isArray(names) && names.length) {
                                        let elapsed = +new Date() - c.created;
                                        let nsc = (100 / c.replies) * (names.length / 100);
                                        if (nsc < 0.35 || ((elapsed > 1000 * 60 * 60 * 6) && names.length < 10) || c.replies >= 300) {
                                            r();
                                            return;
                                        }
                                        c.confidence += nsc / 4;
                                        c.confidence += ((300 / 100) * ((300 - c.replies) / 300)) / 3 / 4;
                                        targets.push(c);
                                    }
                                }
                                r();
                            });
                    }, 1000);
                });
            }), Promise.resolve());
            resolve(targets);
        });
    }

}

var network = new net_work();
network.findThread(INDICATOR_TOKENS)
    .then(threads => {
        if (threads === APP_ERROR) return;
        network.filterByNameSync(threads)
            .then(nsres => {
                // results should be a collection of threads most likely to be /b/read
                // highest confidence score should be the most likely active thread
                console.log(nsres);
            });
    });
